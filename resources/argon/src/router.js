import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from './layout/DashboardLayout'
import AuthLayout from './layout/AuthLayout'
import Dashboard from './views/Dashboard.vue'
import Icons from './views/Icons.vue'
import Profile from './views/UserProfile.vue'
import Maps from './views/Maps.vue'
import Tables from './views/Tables.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: Dashboard,
        },
        {
          path: '/icons',
          name: 'icons',
          component: Icons
        },
        {
          path: '/profile',
          name: 'profile',
          component: Profile
        },
        {
          path: '/maps',
          name: 'maps',
          component: Maps
        },
        {
          path: '/tables',
          name: 'tables',
          component: Tables
        }
      ]
    },
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: Login
        },
        {
          path: '/register',
          name: 'register',
          component: Register
        }
      ]
    }
  ]
})
